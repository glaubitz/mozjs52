#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

DEB_HOST_ARCH  ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)
DEB_HOST_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
DEB_HOST_ARCH_ENDIAN ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_ENDIAN)
# mozjs' build process does not seem to be compatible with other shells
# like zsh
export SHELL = /bin/sh
export PYTHON = python

SRCDIR = $(CURDIR)/js/src
CONFIGURE_FLAGS =

# ia64 currently has toolchain issues, so relax the link optimization
# -fno-schedule-insns2 breaks gcc on ia64, so leave it enabled
ifneq (,$(findstring $(DEB_BUILD_ARCH),ia64))
	DEB_CFLAGS_MAINT_APPEND += -G0 -fno-lifetime-dse -fno-delete-null-pointer-checks
	DEB_CXXFLAGS_MAINT_APPEND += -G0 -fno-lifetime-dse -fno-delete-null-pointer-checks
else
	DEB_CFLAGS_MAINT_APPEND += -fno-schedule-insns2 -fno-lifetime-dse -fno-delete-null-pointer-checks
	DEB_CXXFLAGS_MAINT_APPEND += -fno-schedule-insns2 -fno-lifetime-dse -fno-delete-null-pointer-checks
endif
ifneq (,$(findstring $(DEB_BUILD_ARCH),armel armhf))
        DEB_CFLAGS_MAINT_APPEND += -fno-schedule-insns
        DEB_CXXFLAGS_MAINT_APPEND += -fno-schedule-insns
endif
export DEB_CFLAGS_MAINT_APPEND DEB_CXXFLAGS_MAINT_APPEND

ifeq ($(DEB_HOST_ARCH),mips)
CONFIGURE_FLAGS += --disable-ion
endif

ifeq ($(DEB_HOST_ARCH_ENDIAN),little)
ICU_DATA_FILE = icudt58l.dat
else
ICU_DATA_FILE = icudt58b.dat
endif
export ICU_DATA_FILE

%:
	dh $@ --sourcedirectory=$(SRCDIR) --with gnome,pkgkde-symbolshelper --without autoreconf

override_dh_clean:
	dh_clean
	find $(CURDIR)/js/src/ \( -type l  -o -name \*.pyc \) -exec rm {} \;
	rm -f $(CURDIR)/js/src/config/nsinstall
	rm -f $(CURDIR)/js/src/dist/bin/.purgecaches
	rm -f  $(CURDIR)/js/src/js52-config $(CURDIR)/js/src/mozjs-52.pc \
		$(CURDIR)/js/src/symverscript
	rm -f $(CURDIR)/js/src/config.log
	# rm -f $(CURDIR)/js/src/configure

# dh_auto_configure does not properly handle autoconf2.13 generated configure
# scripts, so we call configure ourselves.
# http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=570375
# The bundled copy of icu prefers clang, which we need to override to
# use gcc because clang doesn't support our DEB_CFLAGS_MAINT_APPEND
override_dh_auto_configure:
	CC=$${CC:-$(DEB_HOST_GNU_TYPE)-gcc} \
	CXX=$${CXX:-$(DEB_HOST_GNU_TYPE)-g++} \
	VERBOSE=1 python intl/icu_sources_data.py "$(CURDIR)"
	cd $(SRCDIR) && $(SHELL) configure \
		--host=$(DEB_HOST_GNU_TYPE) \
		--target=$(DEB_BUILD_GNU_TYPE) \
		--prefix=/usr \
		--libdir=/usr/lib/${DEB_HOST_MULTIARCH}/ \
		--without-system-icu \
		--enable-posix-nspr-emulation \
		--with-system-zlib \
		--enable-tests \
		--disable-strip \
		--enable-threadsafe \
		--with-intl-api \
		--enable-readline \
		--enable-xterm-updates \
		--enable-shared-js \
		--enable-gcgenerational \
		--disable-optimize \
		--enable-pie \
		$(CONFIGURE_FLAGS)

override_dh_install:
	# move library and create additional symlinks to standardize the file layout
	cd $(CURDIR)/debian/tmp/usr/lib/${DEB_HOST_MULTIARCH}/ && \
		mv libjs_static.ajs libmozjs-52.a && \
		mv libmozjs-52.so libmozjs-52.so.0.0.0 && \
		ln -s libmozjs-52.so.0.0.0 libmozjs-52.so.0 && \
		ln -s libmozjs-52.so.0 libmozjs-52.so
	rm debian/tmp/usr/lib/${DEB_HOST_MULTIARCH}/libmozjs-52.a
	dh_install

override_dh_missing:
	dh_missing --fail-missing

override_dh_fixperms:
	dh_fixperms
	chmod a-x $(CURDIR)/debian/libmozjs-52-dev/usr/lib/${DEB_HOST_MULTIARCH}/pkgconfig/mozjs-52.pc

override_dh_gnome:
	dh_gnome --no-gnome-versions

override_dh_auto_test:
	@:
ifeq ($(DEB_HOST_GNU_TYPE),$(DEB_BUILD_GNU_TYPE))
ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	SRCDIR=${SRCDIR} DEB_HOST_ARCH=${DEB_HOST_ARCH} $(CURDIR)/debian/test.sh
endif
endif
